#include<stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<string.h>
#include <arpa/inet.h>

#define BANCO_ADDR "127.0.0.1"
#define BANCO_PORT 6782
#define STD_BUF 1024

struct sockaddr_in serveraddr; 
char buf[1024];
int sockid;


int menu() {
  int opt = -1;
  while(opt < 1 || opt > 4) {
    system("clear");
    printf("****************************\n");
    printf("Buen dia, por favor seleccion una opcion\n");
    printf("1 - Consulta Saldo\n");
    printf("2 - Deposito\n");
    printf("3 - Extraccion\n");
    printf("4 - salir\n");
    printf("Sleccione: ");
    scanf("%i",&opt);
  }
  return opt;
}


void inicia_conexion() {
  while(1) {
    printf("Conectando con el banco\n");
    printf("Armando socket\n");
    sockid = socket(AF_INET,SOCK_STREAM,0);
    if(sockid == -1 ) {
      perror("Error creando socket");
      sleep(2);
      continue;
    }
    
    printf("Socket creado\n");
    
    bzero((char *)&serveraddr,sizeof(serveraddr));

    serveraddr.sin_family = AF_INET;
    serveraddr.sin_port=htons(BANCO_PORT);
    serveraddr.sin_addr.s_addr=inet_addr(BANCO_ADDR);

    if( connect(sockid, (struct sockaddr *)&serveraddr, sizeof(serveraddr)) < 0){
      printf("\n Error : Connect Failed \n");
      close(sockid);
      sleep(2);
      continue;
    }
    break;
  }
}

void do_comando(char **resp, char *comando, char *arg1, char *arg2) {
  char cmd[1024];
  bzero(&cmd, sizeof(cmd));
  strcpy(cmd, comando);
  strcat(cmd, "|");
  if(arg1 != NULL) {
    strcat(cmd, arg1);
  }
  if(arg2 != NULL) {
    strcat(cmd, "|");
    strcat(cmd, arg2);
  }
  strcat(cmd, "\n");
  send(sockid, cmd, strlen(cmd),0);
  char *resp_raw = (char*) malloc(STD_BUF*sizeof(char));
  bzero(resp_raw, sizeof(resp_raw));
  char buff[256];
  int i =0;
  int in;
  bool finished = false;
  while(!finished) {
    in = read(sockid, buff, sizeof(buff) - 1);
    for(int j = 0; j < in; j++) {
      if(buff[i] == '\n') {
        finished = true;
        break;
      }
      resp_raw[i] = buff[j];
      i++;
    }
  }
  char * tofree = resp_raw;
  char * token;
  int tokens = 0;
  //Hacemos el split
  while ((token = strsep(&resp_raw, "|")) != NULL){
    tokens++;
    resp = (char**)realloc(resp, tokens*sizeof(char*));
    resp[tokens-1] = token;
  }
  free(tofree);
  
}

void do_auth() {
  char *tarjeta, *pin;
  tarjeta =(char*) malloc(50*sizeof(char));
  pin =(char*) malloc(50*sizeof(char));
  int logged_in = 0;
  while(!logged_in) {
    char ** resp = (char**)(malloc(sizeof(char*)));
    system("clear");
    printf("Tarjeta: ");
    scanf("%s",tarjeta);
    printf("Pin: ");
    scanf("%s", pin);
    do_comando(resp,"AUTH",tarjeta,pin);
    if(strcmp(resp[0],"OK") == 0 ) {
      logged_in = 1;
    } else {
      printf("Usuario invalido\nEspera para volver a loguearse\n");
      sleep(2);
    }
    free(resp);
  }
}
void mypause() { 
  char c = NULL;
  while(c != '\n'){
    printf("Presione enter para continuar\n");
    scanf("%c",&c);	
    getchar();
  }
} 
void do_consulta() {
  system("clear");
  char ** resp = (char**)(malloc(sizeof(char*)));
  char *a;
  do_comando(resp, "SALDO", NULL, NULL);
  fflush(stdout);
  fflush(stdin);
  printf("Su saldo actual es: %s\n", resp[1]);
  mypause();
}

void do_deposito() {
  float f;
  char buf[100];
  char ** resp = (char**)(malloc(sizeof(char*)));
  system("clear");
  printf("Cantidad: ");
  scanf("%f", &f);
  sprintf(buf,"%f", f);
  do_comando(resp, "DEPOSITO", buf, NULL);
  if(strcmp(resp[0], "ERROR") == 0) {
    printf("Ocurrio un problema: %s", resp[1]);
  } else {
    printf("Deposito realizado con exito\n");
  }
  mypause();
}

void do_extraccion() {
  float f;
  char buf[100];
  char ** resp = (char**)(malloc(sizeof(char*)));
  system("clear");
  printf("Cantidad: ");
  scanf("%f", &f);
  sprintf(buf,"%f", f);
  do_comando(resp, "EXTRACCION", buf, NULL);
  if(strcmp(resp[0], "ERROR") == 0) {
    printf("Ocurrio un problema: %s", resp[1]);
  } else {
    printf("Extraccion realizada con exito\n");
  }
  mypause();
}
void do_logout() {
  close(sockid);
}

int main(int argc, char** args) {
  int menu_opt;
  while(true) {
    inicia_conexion();
    do_auth();
    menu_opt = -1;
    while(menu_opt != 4) {
      menu_opt = menu();
      if(menu_opt == 1) {
        do_consulta();
      } else if(menu_opt == 2) {
        do_deposito();
      } else if(menu_opt == 3) {
        do_extraccion();
      }
    }
    do_logout();
  }
}
