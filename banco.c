#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>

#define MAX_CLIENTS 100
#define STD_BUFF 1024

/**
 * El protocolo que vamos a usar es
 * COMANDO|[Arg1|][Arg2|][ArgN|]
 *
 * El protocolo funciona en modo request->response sobre conexiones autenticadas
 *
 * Las acciones soportadas son:
 *  - AUTH|Numero Tarjeta|Pin: esta es la primera opearcion a realizar y se encargara de autenticar la conexion
 *  - DEPOSITO|monto: va a cargar dinero en la cuenta del usuario logueado
 *  - EXTRACCION|monto: va retirar dinero de la cuenta del cliente
 *  - SALDO|: va a devolver el saldo del cliente
 *  - QUIT|: finalizará la seción del cliente
 */
typedef struct usuario {
  int tarjeta;
  float saldo;
  int pass;
} usuario;

typedef struct cajero_conn_data {
  int fd;
  bool auth;
  char * actual_command;
  int writed;
  usuario * u;
} ccon;


usuario ** usuarios;
int usuarios_c;
fd_set fds, readfds;

void init_usuarios() {
  usuarios_c = 5;
  usuarios = (usuario**)malloc(usuarios_c*sizeof(usuario*));
  usuarios[0] = (usuario*)malloc(usuarios_c*sizeof(usuario));
  usuarios[0]->tarjeta = 1111;
  usuarios[0]->saldo = 1150;
  usuarios[0]->pass = 1234;

  usuarios[1] = (usuario*)malloc(usuarios_c*sizeof(usuario));
  usuarios[1]->tarjeta = 2222;
  usuarios[1]->saldo = 33;
  usuarios[1]->pass = 1234;
  
  usuarios[2] = (usuario*)malloc(usuarios_c*sizeof(usuario));
  usuarios[2]->tarjeta = 3333;
  usuarios[2]->saldo = 34900;
  usuarios[2]->pass = 1234;

  usuarios[3] = (usuario*)malloc(usuarios_c*sizeof(usuario));
  usuarios[3]->tarjeta = 4444;
  usuarios[3]->saldo = 0;
  usuarios[3]->pass = 1234;

  usuarios[4] = (usuario*)malloc(usuarios_c*sizeof(usuario));
  usuarios[4]->tarjeta = 5555;
  usuarios[4]->saldo = 768;
  usuarios[4]->pass = 1234;

}

/**
 * Cierra una conexion
 */
void close_conn(int fd) {
  printf("Cerrando conexion [%i]\n",fd);
  FD_CLR(fd, &fds);
  close(fd);
  printf("Conexion cerrada\n");
}

/**
 * Metodos de envio de datos genericos
 */

/**
 * Envia un error al cliente
 */
void send_error(ccon * conexion, char * msg) {
  char error[1024];
  bzero(&error, sizeof(error));
  strcpy(error, "ERROR|");
  strcat(error, msg);
  strcat(error, "\n");
  write(conexion->fd, error, strlen(error));  
}

/**
 * Envia confirmacion de OK al usuario
 * */
void send_ok(ccon * conexion, char * msg) {
  char ok[1024];
  bzero(&ok, sizeof(ok));
  strcpy(ok, "OK|");
  strcat(ok, msg);
  strcat(ok, "\n");
  write(conexion->fd, ok, strlen(ok));  
}

/**
 * Opearciones
 */

/**
 * Realiza el proceso de autenticación de la conexion
 */
void doAuth(ccon * conexion, char ** comando, int comando_c) {
  if(comando_c != 3) {
     send_error(conexion, "Cantidad de parametros invalidos para comando AUTH");
  } else {
    int tarjeta = atoi(comando[1]);
    int pass = atoi(comando[2]); 
    for(int i = 0; i < usuarios_c; i++) {
      if(usuarios[i]->tarjeta == tarjeta) {
        if(usuarios[i]->pass == pass) {
          conexion->u = usuarios[i];
          conexion->auth = true;
          send_ok(conexion, "LOGGED_IN");
        } else {
          send_error(conexion, "Password invalida");
        }
        return;
      }
    }
   send_error(conexion, "Usuario no encontrado"); 
  }
}

/**
 * Realiza el proceso de ingreso de dinero
 */
void doDeposito(ccon * conexion, char ** comando, int comando_c) {
  if(!conexion->auth) {
    send_error(conexion, "La accion requiere estar logueado");
    return;
  }
  if(comando_c != 2) {
    send_error(conexion, "Error formato de DEPOSITO invalido");
    return;
  }
  float f = atof(comando[1]);
  if(f<= 0) {
    send_error(conexion, "Importes negativos no validos");
    return;
  }
  conexion->u->saldo+=f;
  char * s = (char*)malloc(1024*sizeof(char));
  bzero(s, sizeof(s));
  sprintf(s,"%.2f",conexion->u->saldo);
  send_ok(conexion, s);
}

/**
 * Extraccion de dinero
 */
void doExtraccion(ccon * conexion, char ** comando, int comando_c) {
  if(!conexion->auth) {
    send_error(conexion, "La accion requiere estar logueado");
    return;
  }
  if(comando_c != 2) {
    send_error(conexion, "Error formato de DEPOSITO invalido");
    return;
  }
  float f = atof(comando[1]);
  if(f<= 0) {
    send_error(conexion, "Importes negativos no validos");
    return;
  }
  if(conexion->u->saldo < f) {
    send_error(conexion, "Saldo insuficiente");
    return;
  } 
  conexion->u->saldo-=f;
  char * s = (char*)malloc(1024*sizeof(char));
  bzero(s, sizeof(s));
  sprintf(s,"%.2f",conexion->u->saldo);
  send_ok(conexion, s);

}

/**
 * Operacion de cerrado de conexion
 */
void doQuit(ccon * conexion) {
  send_ok(conexion, "CHAU");
  close_conn(conexion->fd);
  conexion->auth = false;
  free(conexion);
}

/**
 * Obtiene el saldo
 */
void doSaldo(ccon * conexion, char ** comando, int comando_c) {
  if(!conexion->auth) {
    send_error(conexion, "La accion requiere estar logueado");
    return;
  }
  char * s = (char*)malloc(1024*sizeof(char));
  bzero((char*)s, sizeof(s));
  sprintf(s,"%.2f",conexion->u->saldo);
  send_ok(conexion, s);

}

/**
 * Procesa un comando cuqlquiera de una conexion, esto procesa de forma generica
 */
void processCommand(ccon * conexion) {
  char * command = strdup(conexion->actual_command);
  //Uso esta porque el strsep me cambia el puntero
  char * tofree = command;
  char * token;
  char ** command_array = (char**)malloc(1*sizeof(char*));
  int tokens = 0;
  //Hacemos el split
  while ((token = strsep(&command, "|")) != NULL){
    tokens++;
    command_array = (char**)realloc(command_array, tokens*sizeof(char*));
    command_array[tokens-1] = token;
  }
  if(tokens <= 0) {
    send_error(conexion, "Formato de mensaje invalido");
  } else {
    //Buscamos la operacion
    if(strcmp(command_array[0], "AUTH") == 0) {
      doAuth(conexion, command_array, tokens);
    } else if(strcmp(command_array[0], "DEPOSITO") == 0) {
      doDeposito(conexion, command_array, tokens);
    }else if(strcmp(command_array[0], "EXTRACCION") == 0) {
      doExtraccion(conexion, command_array, tokens);
    }else if(strcmp(command_array[0], "SALDO") == 0) {
      doSaldo(conexion, command_array, tokens);
    }else if(strcmp(command_array[0], "QUIT") == 0) {
      doQuit(conexion);
    } else {
      send_error(conexion, "MENSAJE INVALIDO");
    }
  }
  //Luego de procesar liberamos todo
  free(conexion->actual_command);
  conexion->actual_command = (char*)malloc(STD_BUFF*sizeof(char));
  conexion->writed = 0;
  free(tofree);
}

/**
 * Parte principal del programa donde vamos a ejecutar el control principal de los sockets
 */
int main(int argc, char **argv) {
  int i, clientaddrlen;
  int rc, numsocks = 0, maxsocks = MAX_CLIENTS;
  int in;
  char buff[256];

  ccon * conexiones[MAX_CLIENTS];

  init_usuarios();
  //Creo el socket
  int serversock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
  printf("Creando socket \n");
  //Si falla salimos
  if (serversock == -1) perror("Socket");

  //Inicializamos la estructura del server
  struct sockaddr_in serveraddr, clientaddr;  
  bzero(&serveraddr, sizeof(struct sockaddr_in));
  serveraddr.sin_family = AF_INET;
  serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
  serveraddr.sin_port = htons(6782);

  //Hacemos el bind
  if (-1 == bind(serversock, (struct sockaddr *)&serveraddr, 
                 sizeof(struct sockaddr_in))) {
    perror("Bind");
    exit(10);
  }

  printf("Escuchando en puerto 6782 \n");
  //Ejecutamos el listen
  if (listen(serversock, SOMAXCONN) == -1)
    perror("Listen");

  //Inicializamos el set de FD
  FD_ZERO(&fds);
  FD_SET(serversock, &fds);

  while(1) {

    readfds = fds;
    //Ejecutamos el selec sin timeout
    rc = select(FD_SETSIZE, &readfds, NULL, NULL, NULL);

    if (rc == -1) {
      perror("Select");
    }
     
    //Para cada uno de los files descriptors
    for (i = 0; i < FD_SETSIZE; i++) {
      //Verificamos si hubo cambios
      if (FD_ISSET(i, &readfds)) {
        //Si el cambio se da sobre el socket principal entonces es una nueva conexion
        if (i == serversock) {
          if (numsocks < maxsocks) {
            //Aceptamos la conexion
            conexiones[numsocks] = (ccon*) malloc(sizeof(ccon));
            conexiones[numsocks]->writed = 0; 
            conexiones[numsocks]->auth = false; 
            conexiones[numsocks]->actual_command = (char*) malloc(STD_BUFF*sizeof(char));
            conexiones[numsocks]->fd = accept(serversock,
                                      (struct sockaddr *) &clientaddr,
                                      (socklen_t *)&clientaddrlen);
            //Si falla el accept pinchamos
            if (conexiones[numsocks]->fd == -1) 
              perror("Accept");
            //Agregamos el socket al array de file descriptors
            FD_SET(conexiones[numsocks]->fd, &fds);
            printf("Nueva conexion de fd [%i]\n", conexiones[numsocks]->fd);
            numsocks++;
          } else {
            printf("No se pueden aceptar mas conexiones\n");

          }
        } else {
          //Era una conexion que ya existia
          ccon * cajero;
          int cajero_pos;
          for(int j=0; j < numsocks; j++) {
            if(conexiones[j]->fd == i) {
              cajero_pos = j;
              cajero = conexiones[j];
              break;
            }
          }       
          in = read(cajero->fd, buff, sizeof(buff) - 1);
         
          if(in == 0) {
             //La conexion termino;
            close_conn(i);            
          } else {
            //Copiamos el buffer
            for(int i=0; i < in; i++) {
              if(buff[i] == '\n') {
                //Encontre que el comando termino, lo proceso y sigo
                processCommand(cajero);
              } else {
                //Copio posicion en el comando actual e incremento la ultima pos
                cajero->actual_command[cajero->writed] = buff[i];
                cajero->writed++;
              } 
            }
          }
        }
      }
    }
  }
  close(serversock);
  return 0;
}
